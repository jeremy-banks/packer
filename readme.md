https://nickcharlton.net/posts/using-packer-esxi-6.html
http://sanbarrow.com/vmx.html
http://faq.sanbarrow.com/index.php?action=artikel&cat=7&id=54&artlang=en
http://wiki.ninjafocus.net/VMWare_Config_Options
https://github.com/hashicorp/packer/issues/2947


#packer build -var-file secrets.json nas.json
#ansible-playbook -i hosts/dev site.yml --limit=nas --extra-vars 'my_hostname=nas' --extra-vars '{'role_groups': ['users','ufw','openssh_server','docker','nfs','samba']}' --extra-vars 'extra_var_nfs=sdb extra_var_smb=sdc'

#packer build -var-file secrets.json ubuntu16.json
#ansible-playbook -i hosts/dev site.yml --limit=ubuntu16 --extra-vars 'my_hostname=ubuntu16' --extra-vars '{'role_groups': ['users','ufw','openssh_server','docker','factorio']}'

#packer build -var-file secrets.json gitlab.json
#ansible-playbook -i hosts/dev site.yml --limit=gitlab --extra-vars 'my_hostname=gitlab' --extra-vars '{'role_groups': ['users','ufw','openssh_server','docker','gitlab']}'

#packer build -var-file secrets.json vault.json
#ansible-playbook -i hosts/dev site.yml --limit=vault --extra-vars 'my_hostname=vault' --extra-vars '{'role_groups': ['users','ufw','openssh_server','docker','vault']}'

#packer build -var-file secrets.json jenkins.json
#ansible-playbook -i hosts/dev site.yml --limit=jenkins --extra-vars 'my_hostname=jenkins' --extra-vars '{'role_groups': ['users','ufw','openssh_server','docker','jenkins']}'

#packer build -var-file secrets.json jenkins_agent.json
#ansible-playbook -i hosts/dev site.yml --limit=jenkins_agent --extra-vars 'my_hostname=jenkins_agent' --extra-vars '{'role_groups': ['users','ufw','openssh_server','docker','jenkins_agent']}'

#packer build -var-file secrets.json splunk.json
#ansible-playbook -i hosts/dev site.yml --limit=splunk --extra-vars 'my_hostname=splunk' --extra-vars '{'role_groups': ['users','ufw','openssh_server','docker','splunk']}'


#tests i used to determined 64k was best blocksize for nfs
#time dd if=/dev/zero of=/tmp/output bs=4k count=16384; rm -f /tmp/output
#time dd if=/dev/zero of=/tmp/output bs=16k count=16384; rm -f /tmp/output
#time dd if=/dev/zero of=/tmp/output bs=64k count=16384; rm -f /tmp/output

#time dd if=/dev/zero of=/var/lib/ubuntu162-hostdata/ubuntu162/output bs=4k count=16384; rm -f /var/lib/ubuntu162-hostdata/ubuntu162/output
#time dd if=/dev/zero of=/var/lib/ubuntu162-hostdata/ubuntu162/output bs=16k count=16384; rm -f /var/lib/ubuntu162-hostdata/ubuntu162/output
#time dd if=/dev/zero of=/var/lib/ubuntu162-hostdata/ubuntu162/output bs=64k count=16384; rm -f /var/lib/ubuntu162-hostdata/ubuntu162/output

#192.168.2.10:/ /var/lib/ubuntu162-hostdata nfs defaults,rsize=16384,wsize=16384 0 0
#192.168.2.10:/ /var/lib/ubuntu162-hostdata nfs defaults,rsize=65536,wsize=65536 0 0
#192.168.2.10:/ /var/lib/ubuntu162-hostdata nfs defaults,rsize=131072,wsize=131072 0 0




#cpu

23408


tot cap     19533
1x          2925        
2x          5852
3x          8778
4x          11704
5x          14630
6x          17556   ---limit
7x          19533

vcpus   cpu resMin  ram
x2      2926        2gb
x4      2926        4gb
x4      5852        8gb
